const persona = {
    nombre: 'Gerardo',
    profesion: 'Desarrollador Web',
    edad: 500
};

function Tarea(nombre, urgencia) {
    this.nombre = nombre;
    this.urgencia = urgencia;
}

Tarea.prototype.mostrarInformacionTarea = function () {
    return `La tarea ${this.nombre} tiene una prioridad de ${this.urgencia}`;
};

const tarea1 = new Tarea('Aprender Javascript y React', 'Urgente');
console.log(tarea1);
console.log(tarea1.mostrarInformacionTarea());
