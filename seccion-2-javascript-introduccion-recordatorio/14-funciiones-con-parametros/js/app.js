//Parametros por default en las funciones

function actividad(nombre = 'Walter White', actividad = 'Enseñar quimica') {
    console.log(`La persona ${nombre}, esta relizando la actividad ${actividad}`)
}

actividad('Gerardo', 'Aprender Javascript');
actividad('Luis', 'Creando sitio');
actividad('Gerry');

const actividad = function (nombre = 'Walter White', actividad = 'Enseñar quimica') {
    console.log(`La persona ${nombre}, esta relizando la actividad ${actividad}`)
};

actividad('Gerardo', 'Aprender Javascript');
actividad('Luis', 'Creando sitio');
actividad('Gerry');
