export const nombreTarea = 'Pasear al perro';

export const crearTarea = (tarea, urgencia) =>{
    return `La tarea ${tarea} tiene una urgencia de ${urgencia}`
};

export const tareaCompletada = () =>{
    console.log('La tarea se completo')
};

//Escribir clases
class Tarea {
    constructor(nombre, prioridad) {
        this.nombre = nombre;
        this.prioridad = prioridad;
    }
    mostrar(){
        return `${this.nombre} tiene una prioridad de ${this.prioridad}`
    }
}

class ComprasPendientes extends Tarea{
    constructor(nombre, prioridad, cantidad) {
        super(nombre, prioridad);
        this.cantidad = cantidad;
    }
}

//Crear los objetos
let tarea1 = new Tarea('Aprender Javascript', 'Alta');

console.log(tarea1.mostrar());

let compra1 = new ComprasPendientes('Jabon', 'Urgentes', 3);
console.log(compra1);
