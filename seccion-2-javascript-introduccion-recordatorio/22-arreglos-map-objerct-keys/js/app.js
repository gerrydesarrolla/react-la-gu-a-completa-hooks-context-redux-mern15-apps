//Arreglos y map
const carrito = ['Producto 1', "Producto 2", "Producto 3"];

console.log(carrito);

const appContenedor = document.querySelector('#app');

let html = '';
carrito.forEach(producto => {
    html += `<li>${producto}</li>`
});

carrito.map(producto => {
    return `El producto es: ${producto}`
});

const persona = {
    nombre: 'Gerardo',
    profesion: 'Desarrollador Web',
    edad: 500
};

console.log(Object.keys(persona));

appContenedor.innerHTML = html;
