//Object literal enhacement
const banda = 'Metallica',
    genero = 'Heave Metal',
    canciones = ['Master of puppets', 'Seek & Destroy', 'Enter Sandman'];

//Forma vieja
const metallica = {
        banda: banda,
        genero: genero,
        canciones: canciones
    };

//Forma nueva
const metallica = {banda, genero, canciones};

console.log(metallica);
