//Metodos o funciones en un objeto
const  persona = {
    nombre: 'Gerardo',
    trabajo: 'Desarrollador Web',
    edad: 500,
    musicaRap: true,
    mostrarInformacion() {
        console.log(`${this.nombre} es ${this.trabajo} y su edad es ${this.edad}`)
    }
};

persona.mostrarInformacion();
