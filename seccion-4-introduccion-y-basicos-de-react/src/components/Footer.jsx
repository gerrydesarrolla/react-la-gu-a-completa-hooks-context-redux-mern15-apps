import React from "react";

function Footer({fecha}) {
    return(
        <footer>
            <p>Todos los derechos reservados {fecha}</p>
        </footer>
    )
}

export default Footer;
