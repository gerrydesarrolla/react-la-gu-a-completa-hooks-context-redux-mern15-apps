//Destructuring de objetos
const aprendiendoJS = {
    version: {
        nueva: 'ES6',
        anterior: 'ES5'
    },
    frameworks: ['React', 'VueJS', 'AngularJS']
};

//Destructuring es extraer valores de un objetos
console.log(aprendiendoJS);

//Version anterior
let version = aprendiendoJS.version.nueva;
let frameword = aprendiendoJS.frameworks[1];
console.log(version);

//Version nueva

let {anterior} = aprendiendoJS.version;
console.log(anterior);
console.log(frameworks);
