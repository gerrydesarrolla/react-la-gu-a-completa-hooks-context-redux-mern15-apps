//creando funcion

//Function Declaration
function saludar(nombre) {
    console.log('Bienvenido ' + nombre);
}

saludar('Gerardo');
saludar('Luis');

//Function expression
const cliente = function (nombreCliente) {
    console.log(`Mostrando nombre del cliente: ${nombreCliente}`)
};

cliente('Gerardo');
