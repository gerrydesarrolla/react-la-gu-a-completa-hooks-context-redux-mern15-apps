//Metodos en arreglos
const personas = [
    {
        nombre: 'Juan',
        edad: 28,
        aprendiendo: 'Javascript'
    },
    {
        nombre: 'Gerardo',
        edad: 32,
        aprendiendo: 'Phyton'
    },
    {
        nombre: 'Ricardo',
        edad: 25,
        aprendiendo: 'React'
    },
    {
        nombre: 'Karen',
        edad: 52,
        aprendiendo: 'Django'
    },
    {
        nombre: 'Miguel',
        edad: 34,
        aprendiendo: 'Angular'
    },
];

//Mayores de 28 años
const mayores = personas.filter(persona =>{
    return persona.edad > 28
});

console.log(mayores);


//Que aprende alejandra
const alejandra = personas.find(persona=>{
    return persona.nombre === 'Karen'
});

console.log(alejandra);

let total = personas.reduce((edadTotal, persona) =>{
    return edadTotal + persona.edad;
}, 0);

console.log(total);
