const descargarUsuarios = cantidad => new Promise((resolve, reject) => {
    const api = `https://randomuser.me/api/?results=${cantidad}&nat=us`;

    //Llamado ajax
    const xhr = new XMLHttpRequest();

    xhr.open('GET', api, true);

    xhr.onload = () => {
        if(xhr.status === 200){
            resolve(JSON.parse(xhr.responseText).results)
        }else{
            reject(Error(xhr.statusText))
        }
    };

    xhr.onerror = error => reject(error);

    xhr.send();
});

descargarUsuarios(20).then(
    miembros => imprimirHTML(miembros),
    error => console.error(new Error(`Hubo un error ${error}`))
);

function inprimirHTML(usuarios) {
    let html = '';
    usuarios.forEach(usuario => {
        console.log(usuario);

        html += `
            <li>Nombre: ${usuario.name.first} ${usuario.name.last}</li>
        `
    })

    const contenedorApp = document.querySelector('#app');
    contenedorApp.innerHTML = html;
}
