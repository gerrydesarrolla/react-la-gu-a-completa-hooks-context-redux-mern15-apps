import React from "react";

function Header({titulo}) {
    return(
        <header>
            <h1 className="encabezado">{titulo}</h1>
        </header>
    )
}

export default Header;
