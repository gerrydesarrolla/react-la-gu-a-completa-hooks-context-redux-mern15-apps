// Crear variables

//Variables con var
var aprendiendo = 'Javascript';
aprendiendo = true;

//Variables con Const
const aprendiendoDos = 'Javascript';

//Variables con Let
let aprendiendoTres = 'Javascript';
aprendiendoTres = true;

//Scope
/*var musica = 'Rock';

if(musica){
    var musica = 'Grunge';

    console.log('dentro del if', musica)
}
console.log('fuera del if', musica);*/

let musica = 'Rock';

if(musica){
    let musica = 'Grunge';

    console.log('dentro del if', musica)
}
console.log('fuera del if', musica);

console.log(aprendiendo);
